
#  Spec Browser

 I am very proud to present my 4th-semester project: Spec Browser. It is a mobile application that I designed and developed exclusively for the Android operating system. It is a browser app that lets you surf the web with ease and convenience. The app was built in the Java environment using the Android Studio software. I also used CSS to create a custom UI design that is attractive and user-friendly. 
Spec Browser is based on the Web View system, which allows you to view web pages as if they were part of your app. You can use it for any casual browsing, such as watching YouTube videos, using Facebook or Instagram, using LinkedIn and many more websites. The app also keeps track of your history and bookmarks, so you can easily access your favorite sites. Spec Browser is a fast, reliable and fun way to explore the internet on your Android device.


## Features

- Light/dark mode toggle
- Live previews
- Desktop/mobile view toggle 
- Cross platform
- History and bookmarks
- Some default quick links


## Tech Used

**Language** Java, CSS

**System** WebView

**Software** Android Studio, Scene Builder


## Feedback

If you have any feedback, please reach out to at http://scr.im/84ta


## Contributing

Contributions are always welcome!

Please adhere to this project's `code of conduct`.

